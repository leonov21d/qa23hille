import pytest

from lesson14.hw14_12_library import Phone, Laptop


@pytest.fixture(scope='class')
def phone():
    phone_data = Phone(
        model='Apple',
        release_date=2023,
        pre_owned_model=True,
        is_not_smartphone=True,
        cost=1000)
    yield phone_data


@pytest.fixture(scope='class')
def laptop():
    data_laptop = Laptop(
        'HP',
        2019,
        pre_owned_model=True,
        cost=20000,
        diagonal=13.3)
    yield data_laptop
