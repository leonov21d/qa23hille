# завдання 2 калькулятор

def input_numb1():
    """Функція перевіря чи введені дані є цифрою, та повертає її,
     :rtype: float or None """
    try:
        numb1 = float(input('Ведіть перше число'))
        return numb1
    except ValueError:
        return None


def input_numb2():
    """Функція перевіря чи введені дані є цифрою, та повертає її,
     :rtype: float or None"""
    try:
        numb2 = float(input('Ведіть друге число'))
        return numb2
    except ValueError:
        return None


def actions():
    """Функція  виконує матиматичні дії з цифрами"""
    act1 = '+'
    act2 = '-'
    act3 = '*'
    act4 = '/'
    act = input('Ввведіть операцію яку треба зробити')
    if act == act1:
        return add
    elif act == act2:
        return remov
    elif act == act3:
        return multipicatiom
    elif act == act4:
        return division
    else:
        act = None
        return act


def add(arg1, arg2):
    """Фукція складає цифри
    :rtype: float
    """
    return arg1 + arg2


def remov(arg1, arg2):
    """Функція вичітає цифри
    :rtype: float
    """
    return arg1 - arg2


def multipicatiom(arg1, arg2):

    return arg1 * arg2


def division(arg1, arg2):
    """Функція  ділить цифри
    """
    return arg1 / arg2


def calculator():
    """Функція склалає введені дані якщо вони цифри
    """
    var1 = input_numb1()
    var2 = input_numb2()
    operation = actions()

    if var1 is None or var2 is None or operation is None:
        print('Невірний тип даних або операція не підтримується')
    else:
        print('Равно {}'.format(operation(var1, var2)))


# calculator()

#Завдання1 Напишіть функцію, яка визначає сезон за датою. Функція отримує стрінг у форматі "[день].[місяць]" (наприклад "12.01", "30.08", "1.11" і тд) і повинна повернути стрінг з відповідним сезоном, до якого відноситься ця дата ("літо", "осінь", "зима", "весна")

my_season = '12.12'


def all_seasons(arg1):
    """Визначає сезон відповідно до місяця"""
    sep_month = int(arg1.split('.')[-1])
    if 3 <= sep_month <= 5:
        return 'Вказаний сезон весна'
    elif 6 <= sep_month <= 8:
        return 'Вказаний сезон літо'
    elif 9 <= sep_month <= 11:
        return 'Вказаний сезон осінь'
    else:
        return 'Вказаний сезон зима'


ses = all_seasons(my_season)
print(ses)