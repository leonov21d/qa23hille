import time

import allure
import pytest
import requests
from allure_commons.types import AttachmentType
from selenium.webdriver import Chrome

from lesson2_16_work import user_data
from lesson2_16_work.config_data import AuthUser, Car
from lesson2_16_work.page import AuthPage


@pytest.fixture(scope='session')
def user(user_session):
    user = AuthUser(email=user_data.person.get('email'), password=user_data.person.get('password'),
                    remember=user_data.person.get('remember'), session=user_session)
    user.user_auth()
    yield user.session


@pytest.fixture(scope='session')
def user_session():
    yield requests.session()


@pytest.fixture(scope='session')
def car(user):
    car_data = Car(car_brand_id=1, car_model_id=1, mileage=122, session=user)
    cars = car_data.get_cars()
    if cars:
        for car_id in cars:
            car_data.delete_car(car_id)

    yield car_data


@pytest.fixture()
def get_browser():
    driver = Chrome(r'D:\Project\qa23hille\Chrome_driver\chromedriver.exe')
    driver.get('https://guest:welcome2qauto@qauto2.forstudy.space')
    yield driver


@pytest.fixture()
def login_auth_user(get_browser):
    browser = get_browser
    auth_page = AuthPage()
    auth_page.click_sign_in(browser)
    auth_page.input_text_field_email(browser, 'ldn@test.com')
    auth_page.input_text_field_password(browser, 'Qwerty12345')
    auth_page.click_btn_login(browser)
    yield browser


def create_screenshot(driver):
    screen_name_using_current_time = time.strftime("%Y%m%d-%H%M%S")
    allure.attach(driver.get_screenshot_as_png(), name=screen_name_using_current_time, attachment_type=AttachmentType.PNG)
