from threading import Thread
from time import sleep


def sale(name_people, range_value):
    for work in range(range_value):
        print(f'using INNER thread {work} {name_people}')
        sleep(2)


my_thread = Thread(target=sale, daemon=True, args=("apples",), kwargs={"range_value": 2})
my_thread.start()

for work in range(8):
    print(f'using Main thread {work}  tomate')
    sleep(0.5)
