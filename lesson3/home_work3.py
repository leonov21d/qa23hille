#     №1 #Напишіть цикл, який буде вимагати від користувача ввести слово, в якому є буква "о" (враховуються як великі так і маленькі).
# #  #Цикл не повинен завершитися, якщо користувач ввів слово без букви "о".
# #


bool_t_o = False
while not bool_t_o:
    t = input('Input word, there is letter o/O in word "').lower()
    bool_t = t.isalpha()
    if bool_t:
        if 'o' in t:
            bool_t_o = True
else:
    print('Thank you')


#     №2 Є list з даними lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']. Напишіть код, який свормує новий list
#  (наприклад lst2), який містить лише змінні типу стрінг, які присутні в lst1. Зауважте, що lst1 не є статичним і може формуватися динамічно
#  від запуску до запуску.
lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
lst2 =[]
for i in lst1:
    if type(i) == str:
        lst2.append(i)
print('New list2 ={}'.format(lst2))


# №3 Є стрінг з певним текстом (можна скористатися input або константою). Напишіть код, який визначить кількість слів в цьому тексті, які
#  закінчуються на "о" (враховуються як великі так і маленькі).
words = 'differentO 45 words. twO, october! O  go nO OslO'
num = 0
for element in  words.lower().split():
    ere = element.rstrip(',''!')
    if ere.endswith('o') is True:
        num +=1
print(num)





  



