import datetime
import pytest

current_year = datetime.datetime.now().year


class TestPositiveCostPhone:

    def test_phone_new_actual_cost(self, phone):
        assert phone.cost == 1000

    def test_cost_with_discount_phone_20_per(self, phone):
        assert phone.cost_with_discount_phone == 800

    def test_cost_with_discount_phone_0_per(self, phone):
        phone.is_not_smartphone = False
        assert phone.cost_with_discount_phone == phone.cost

    @pytest.mark.parametrize('year', [2, 3, 4])
    def test_cost_with_discount_phone_5_per(self, phone, year):
        phone.pre_owned_model = False
        phone.release_date = current_year - year
        assert phone.cost_with_discount_phone == 950

    @pytest.mark.parametrize('year', [4.001, 5, 10])
    def test_cost_with_discount_phone_15_per(self, phone, year):
        phone.is_not_smartphone = False
        phone.release_date = current_year - year
        assert phone.cost_with_discount_phone == 850


class TestNegativeCostPhone:
    def test_negative_cost_0(self, phone):
        phone.cost = 0
        assert phone.cost_with_discount_phone == 0

    def test_negative_cost_negative(self, phone):
        phone.is_not_smartphone = False
        phone.cost = -3
        assert phone.cost_with_discount_phone == -3

    def test_negative_year_negative(self, phone):
        phone.is_not_smartphone = False
        phone.cost = 1000
        phone.release_date = -500
        assert phone.cost_with_discount_phone == 850  # discount 15%
