from pony.orm import db_session, left_join

from home_work_20.models import Users, Role


class UserRepository:
    def __init__(self):
        self.__model = Users

    @db_session
    def get_all_by_lambda(self):
        users = Users.select(lambda role: role)
        return users.prefetch(Role).page(1).to_list()

    @db_session
    def get_user_by_oldest_than_age(self, age):
        users = Users.select(lambda u: u.age > age)
        return users.page(1).to_list()

    @db_session
    def left_join(self):
        results = left_join((user, role) for user in Users for role in user.role)
        for result in results:
            user = result[0]
            role = result[1]
            print(f"{user}||||{role}")


if __name__ == '__main__':
    user_repo = UserRepository()
    # result_2 = user_repo.get_by_id(1)
    print(user_repo.get_user_by_oldest_than_age(15))
