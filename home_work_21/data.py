from pymongo import MongoClient
import json

mongo_client = MongoClient("mongodb://localhost:27017")

db = mongo_client["hille_db"]

collection = db["catalog_collection_6"]


class History:
    def __init__(self, nameowner: str, phone: str, year: int):
        self.nameowner: str = nameowner,
        self.phone: str = phone,
        self.year: int = year


class Cars:
    def __init__(self, model: str, prise: int, color: str, history: History):
        self.model: str = model,
        self.prise: int = prise,
        self.color: str = color,
        self.history: history


class Person:
    def __init__(self, _id: str, name: str, email: str, age: int, cars: list[Cars]):
        self._id = _id,
        self.name: str = name,
        self.email: str = email,
        self.age: int = age,
        self.cars: list = cars

    def __str__(self):
        return f"{self._id} {self.name} {self.email} {self.age} {len(self.cars)}"


def convert_person_to_object(base_dict: dict) -> Person:
    list_of_cars = base_dict["cars"]
    cars_list = list()
    for car in list_of_cars:
        n = car["history"]["nameowner"]
        p = car["history"]["phone"]
        y = car["history"]["year"]
        history_for_car = History(car["history"]["nameowner"], car["history"]["phone"], car["history"]["year"])
        car_in_list = Cars(car["model"], car["prise"], car["color"], history_for_car)
        cars_list.append(car_in_list)
        result_person = Person(base_dict["_id"], base_dict["name"], base_dict["email"], base_dict["age"], cars_list)
        return result_person


with open("data.json") as file:
    file_data = json.load(file)

collection.insert_many(file_data)

person_for_moving_to_object = collection.find_one({"name": "igor"})


print(convert_person_to_object(person_for_moving_to_object))
