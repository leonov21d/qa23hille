from abc import ABC, abstractmethod
import datetime

current_year = datetime.datetime.now().year


class Gadget(ABC):
    def __init__(self, model: str, release_date: int, pre_owned_model: bool, cost: float):
        self.model = model.upper()
        self.release_date = release_date
        self.pre_owned_model = pre_owned_model
        self.cost = cost

    def __str__(self):
        return f'{self.model}  {self.release_date}  {self.pre_owned_model} {self.cost}'

    @abstractmethod
    def get_additional_info(self):
        pass

    @property
    def model_gadget(self):
        return self.model


class Phone(Gadget):
    def __init__(self, model: str, release_date: int, pre_owned_model: bool, cost: float,
                 is_not_smartphone: bool) -> object:
        super().__init__(model, release_date=release_date, pre_owned_model=pre_owned_model,
                         cost=cost)
        self.is_not_smartphone = is_not_smartphone

    @property
    def cost_with_discount_phone(self):
        interest = 1
        if self.is_not_smartphone:
            interest -= 0.2  # discount 20%
        if 2 <= (current_year - self.release_date) <= 4:
            interest -= 0.05  # discount +5%
        elif 4 < (current_year - self.release_date):
            interest -= 0.15  # discount +15%
        return self.cost * interest

    def get_additional_info(self):
        return f'Model  {self.model} cost is {self.cost}'


class Laptop(Gadget):
    def __init__(self, model: str, release_date: int, pre_owned_model: bool, cost: float, diagonal: float) -> object:
        super().__init__(model=model, release_date=release_date, pre_owned_model=pre_owned_model, cost=cost)
        self.diagonal = diagonal

    @property
    def cost_with_discount_laptop(self):
        if self.model.lower() == "apple" and self.diagonal > 15:
            self.cost *= 1  # without discount
        else:
            self.cost *= 0.95  # discount 5%
        return self.cost

    def get_additional_info(self):
        return f'Model  {self.model}  self {self.cost}'


my_laptop = Laptop(
        'HP',
        2019,
        pre_owned_model=True,
        cost=20000,
        diagonal=13.3)

