import time


def time_of_function(function):
    '''Вимірює час роботи функції'''
    def wrapped(*args):
        start_time = time.perf_counter_ns()
        res = function(*args)
        print(time.perf_counter_ns() - start_time)
        return res

    return wrapped
