import time

import pytest
import requests

from lesson2_14.config import RegUser, AuthUser, DelUser

num = str(round(time.time() * 1000))
email = f'p{num}@test.com'


@pytest.fixture(scope='class')
def registration():
    reg_user_data = RegUser(name='Sara', email=email, last_name='Petrov', password="Qwerty12345",
                            repeat_password="Qwerty12345")

    yield reg_user_data


@pytest.fixture(scope='class')
def authorization():
    auth_user_data = AuthUser(email=email, password="Qwerty12345", remember=False)
    yield auth_user_data


@pytest.fixture(scope='class')
def authorization_invalid_password():
    auth_user_inv_password = AuthUser(email=email, password="0", remember=False)
    yield auth_user_inv_password


@pytest.fixture(scope='class')
def authorization_without_password():
    test_auth_without_pass = AuthUser(email=email, password="", remember=False)
    yield test_auth_without_pass


@pytest.fixture(scope='class')
def authorization_without_data():
    test_auth_without_data = AuthUser(email='', password="", remember=False)
    yield test_auth_without_data


@pytest.fixture(scope='class')
def user_session():
    yield requests.session()
