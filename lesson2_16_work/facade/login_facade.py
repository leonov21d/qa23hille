import allure

from lesson2_16_work.facade.facade_bace import FacadeBase


class LoginFacade(FacadeBase):
    def __init__(self):
        super().__init__()

    @allure.step("Click sign in")
    def click_sign_in(self, driver,):
        self._auth_page.click_sign_in(driver)

    @allure.step("Fill email and password fields and click login button")
    def set_email_and_password_and_click_login_button(self, driver,  email, password):
        self._auth_page.input_text_field_email(driver, email)
        self._auth_page.input_text_field_password(driver, password)

    @allure.step("Click login button")
    def click_login_button(self, driver):
        self._auth_page.click_btn_login(driver)

