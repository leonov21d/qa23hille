import locale
from datetime import datetime
import requests
import json


def exchange_rates():
    'creates a file with the exchange rate for today'
    nbu_path = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchangenew?json'

    try:
        date_now = str(datetime.now())[:19]
        date_str = datetime.strptime(date_now, '%Y-%m-%d %H:%M:%S').strftime('%d-%m-%Y %H:%M:%S')

        nbu_course = requests.get(nbu_path)
        all_cource = json.loads(nbu_course.content.decode())

        if nbu_course.status_code == 200:
            if nbu_course.headers.get('Content-Type') == 'application/json; charset=utf-8':
                with open('Currency.txt', 'w', encoding=locale.getpreferredencoding(False)) as file:
                    file.write('Дата створення запиту: {date_str}\n'.format(date_str=date_str))
                    for num, cur in enumerate(all_cource):
                        file.write(
                            '{num}. {cc} to UAH: {rate}\n'.format(num=num, cc=cur.get('cc'), rate=cur.get('rate')))
    except Exception as e:
        print('Помилка: {}'.format(e))


if __name__ == '__main__':
    exchange_rates()
