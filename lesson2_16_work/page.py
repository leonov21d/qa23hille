import time

from selenium.webdriver.common.by import By


class AuthPage:
    btn_sign_in = (By.XPATH, '//div/button[text()="Sign In"]')
    field_email = (By.ID, 'signinEmail')
    field_password = (By.ID, 'signinPassword')
    btn_login = (By.XPATH, '//div/button[text()="Login"]')

    def click_sign_in(self, driver):
        driver.find_element(*self.btn_sign_in).click()

    def input_text_field_email(self, driver, email):
        driver.find_element(*self.field_email).send_keys(email)

    def input_text_field_password(self, driver, passwd):
        driver.find_element(*self.field_password).send_keys(passwd)

    def click_btn_login(self, driver):
        driver.find_element(*self.btn_login).click()


class CarPage:
    btn_add_car = (By.XPATH, '//div/button[text()="Add car"]')
    form_add_car = (By.CLASS_NAME, 'modal-content')
    field_mileage = (By.ID, 'addCarMileage')
    btn_add = (By.XPATH, '//div/button[text()="Add"]')
    btn_edit = (By.CSS_SELECTOR, '.icon-edit.icon-edit')
    form_adit_car = (By.CLASS_NAME, 'modal-content')
    btn_remove_car = (By.XPATH, '//div/button[text()="Remove car"]')
    btn_remove = (By.XPATH, '//div/button[text()="Remove"]')
    panel_page_empty = (By.CLASS_NAME, 'panel-page_empty')

    def click_btn_add_car(self, driver):
        driver.find_element(*self.btn_add_car).click()

    def input_text_field_mileage(self, driver):
        driver.find_element(*self.field_mileage).send_keys('250')

    def click_btn_add(self, driver):
        driver.find_element(*self.btn_add).click()

    def click_btn_edit(self, driver):
        time.sleep(1)
        driver.find_element(*self.btn_edit).click()

    def click_btn_remove_car(self, driver):
        time.sleep(1)
        driver.find_element(*self.btn_remove_car).click()

    def click_btn_remove(self, driver):
        time.sleep(1)
        driver.find_element(*self.btn_remove).click()

    def check_panel_page_empty(self, driver):
        time.sleep(1)
        return driver.find_element(*self.panel_page_empty).is_enabled()
