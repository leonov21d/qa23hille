import time

from lesson2_16_work.facade.login_facade import LoginFacade
from lesson2_16_work.page import CarPage
from lesson2_16_work.tests.conftest import create_screenshot


class TestCar:
    login_facade =LoginFacade()
    car_id_list = []
    car_page = CarPage()

    def test_add_car(self, car):
        self.car_id_list.append(car.add_car())
        assert type(self.car_id_list[0]) == int

    def test_edit_car(self, car):
        mileage = 300
        car.mileage = mileage
        result = car.edit_car(self.car_id_list[0])
        assert result.status_code == 200
        assert result.json().get('data').get('mileage') == mileage

    def test_remove_car(self, car, login_auth_user):
        browser = login_auth_user
        self.car_page.click_btn_edit(browser)
        self.car_page.click_btn_remove_car(browser)
        self.car_page.click_btn_remove(browser)
        assert self.car_page.check_panel_page_empty(browser)
        create_screenshot(browser)

    def test_auth(self, get_browser):
        browser = get_browser
        self.login_facade.click_sign_in(browser)
        self.login_facade.set_email_and_password_and_click_login_button(browser, 'ldn@test.com', 'Qwerty12345')
        self.login_facade.click_login_button(browser)
        create_screenshot(browser)
