
from lesson2_14.config import DelUser


class TestRegAuth:

    def test_reg(self, registration, user_session):
        result = registration.user_reg(user_session)
        assert (result.json()["status"] == 'ok')

    def test_reg_invalid(self, registration, user_session):
        result = registration.user_reg(user_session)
        assert (result.json()["status"] == 'error')

    def test_auth(self, authorization, user_session):
        result = authorization.user_auth(user_session)
        assert (result.json()["status"] == 'ok')

    def test_auth_invalid_pass(self, authorization_invalid_password, user_session):
        result = authorization_invalid_password.user_auth(user_session)
        assert (result.json()["status"] == 'error')

    def test_auth_without_pass(self, authorization_invalid_password, user_session):
        result = authorization_invalid_password.user_auth(user_session)
        assert (result.json()["status"] == 'error')

    def test_auth_without_data(self, authorization_invalid_password, user_session):
        result = authorization_invalid_password.user_auth(user_session)
        assert (result.json()["status"] == 'error')

    def test_delete(self, user_session):
        result = DelUser.user_del(user_session)
        assert (result.status_code == 200)


