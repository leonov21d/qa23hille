class Point:

    def __init__(self, x_coord, y_coord):
        if not isinstance(x_coord, (int, float)):
            raise TypeError
        self._x_coord = x_coord

        if not isinstance(y_coord, (int, float)):
            raise TypeError
        self._y_coord = y_coord

    @property
    def x_coord(self):
        return self._x_coord

    @x_coord.setter
    def x_coord(self, value):
        if not isinstance(value, (int, float)):
            raise TypeError
        self._x_coord = value

    @property
    def y_coord(self):
        return self._y_coord

    @y_coord.setter
    def y_coord(self, value):
        if not isinstance(value, (int, float)):
            raise TypeError
        self._y_coord = value


class Line:
    def __init__(self, begin, end):
        self.begin_x = begin.x_coord
        self.begin_y = begin.y_coord
        self.end_x = end.x_coord
        self.end_y = end.y_coord
        self.length = self._length()

    def __str__(self):
        return f'Line with coords [{self.begin_x}-{self.begin_y}:{self.end_x}-{self.end_y}]'

    def _length(self):
        k1 = self.begin_x - self.end_x
        k2 = self.begin_y - self.end_y
        return (k1 ** 2 + k2 ** 2) ** 0.5

    def __eq__(self, other):  # ==
        if not isinstance(other, type(self)):
            raise TypeError

        is_length_match = self.length == other.length
        return is_length_match

    def __ne__(self, other):  # !=
        if not isinstance(other, type(self)):
            raise TypeError

        is_length_match = self.length != other.length
        return is_length_match

    def __lt__(self, other):  # <
        if not isinstance(other, type(self)):
            raise TypeError

        is_length_match = self.length < other.length
        return is_length_match

    def __le__(self, other):  # <=
        if not isinstance(other, type(self)):
            raise TypeError

        is_length_match = self.length <= other.length
        return is_length_match

    def __gt__(self, other):  # >
        if not isinstance(other, type(self)):
            raise TypeError

        is_length_match = self.length > other.length
        return is_length_match

    def __ge__(self, other):  # >=
        if not isinstance(other, type(self)):
            raise TypeError

        is_length_match = self.length >= other.length
        return is_length_match


if __name__ == '__main__':
    p1 = Point(10.4, 138.2)
    p2 = Point(10, 19.5)
    p3 = Point(20.4, 138.2)
    p4 = Point(10, 19.5)

    print('--- Point ---')
    print(1, p1.x_coord)
    print(2, p1.y_coord)
    p1.x_coord = 30
    p1.y_coord = 15
    print(3, p1.x_coord)
    print(4, p1.y_coord)

    l1 = Line(p1, p2)
    l2 = Line(p3, p4)
    print('--- Line ---')
    print(l1)
    print(l1.length)
    print(l2)
    print(l2.length)
    print(l1 < l2)
