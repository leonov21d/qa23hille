# Завдання 1    Напишіть функцію, яка приймає два аргументи.Якщо обидва аргумени відносяться до числових типів функція пермножує ці аргументи і повертає результат
# Якшо обидва аргументи відносяться до типу стрінг функція обʼєднує їх в один і повертає
# В будь-якому іншому випадку - функція повертає кортеж з двох агрументів
def input_data1():
    var1 = input('Input first data')
    return var1

def input_data2():
    var2 = input('Input second data')
    return var2


def compound_data(arg1, arg2):
    if arg1.isdigit() and arg2.isdigit():
        print('Result compound = {}'.format(float(arg1) * float(arg2)))
    elif arg1.isalpha() and arg2.isalpha():
        print('Result compound = {}'.format(arg1 + arg2))
    else:
        comp_tuple = (arg1, arg2)
        print('Result compound = {}'.format(comp_tuple))

def le():
    var1 = input_data1()
    var2 = input_data2()
    compound_data(var1, var2)


# завдання2   Візьміть попереднє дз "Касир в кінотеатрі" і перепишіть за допомогою функцій
def input_info():
    inspect = False
    while not inspect:
        age1 = input('Дозволяється вводити тільки цифри. Введіть свій вік вірно')
        inspect = bool(age1.isdigit())
    return int(age1)


def cashier_in_the_cinema(age):
    if age < 7:
        t = 'Де твої батьки?'
    elif age % 10 == 7 or age % 100 == 7:
        t = 'Вам сьогодні пощастить!'
    elif age < 16:
        t = 'Це фільм для дорослих!'
    elif age > 65:
        t = 'Покажіть пенсійне посвідчення!'
    else:
        t = 'А білетів вже немає!'
    return t


# age = input_info()
# print(cashier_in_the_cinema(age))

# le()
input_info()
