class TestPositiveCostLaptop:
    def test_laptop_new_actual_cost(self, laptop):
        assert laptop.cost == 20000

    def test_cost_with_discount_laptop_0_per(self, laptop):
        laptop.model = 'apple'
        laptop.diagonal = 17
        assert laptop.cost_with_discount_laptop == 20000

    def test_cost_with_discount_laptop_5_per_little_diagonal(self, laptop):
        laptop.model = 'apple'
        laptop.diagonal = 13
        assert laptop.cost_with_discount_laptop == 19000

    def test_cost_with_discount_laptop_5_per_not_apple(self, laptop):
        laptop.model = 'HP'
        laptop.diagonal = 12
        assert laptop.cost_with_discount_laptop == 18050

    def test_cost_with_discount_laptop_5_per_not_apple_little_diagonal(self, laptop):
        laptop.model = 'HP'
        laptop.diagonal = 12
        assert laptop.cost_with_discount_laptop == 17147.5


class TestNegativeCostLaptop:
    def test_cost_with_discount_laptop_0_per_big_letter(self, laptop):
        laptop.model = 'ApplE'
        laptop.diagonal = 18
        assert laptop.cost_with_discount_laptop == 20000

    def test_cost_with_discount_laptop_0_per_negative_diagonal(self, laptop):
        laptop.model = 'apple'
        laptop.diagonal = -3418
        assert laptop.cost_with_discount_laptop == 19000
