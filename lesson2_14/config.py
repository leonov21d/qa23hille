import json

import requests


class User:
    def __init__(self, email, password):
        self.email = email
        self.password = password


class RegUser(User):
    def __init__(self, email, password, name, last_name, repeat_password):
        super().__init__(password=password, email=email)
        self.name = name
        self.last_name = last_name
        self.repeat_password = repeat_password

    def user_reg(self, ses):
        data = {
            "name": self.name,
            "lastName": self.last_name,
            "email": self.email,
            "password": self.password,
            "repeatPassword": self.repeat_password
        }

        return ses.post(url="https://qauto2.forstudy.space/api/auth/signup",
                            json=data)


class AuthUser(User):
    def __init__(self, email, password, remember):
        super().__init__(password=password, email=email)
        self.remember = remember

    def user_auth(self, ses):
        data = {
            "email": self.email,
            "password": self.password,
            "remember": self.remember

        }


        return ses.post(url="https://qauto2.forstudy.space/api/auth/signin",
                            json=data)


class DelUser:
    @staticmethod
    def user_del(ses):
        return ses.delete(url="https://qauto2.forstudy.space/api/users")
