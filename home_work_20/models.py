from pony.orm import Database, PrimaryKey, Optional, Required, Set

db = Database()
db.bind(provider='postgres', user='postgres', password='admin', host='127.0.0.1', database='hille')


class Role(db.Entity):
    _table_ = "roles"
    role_id = PrimaryKey(int, auto=True)
    title = Required(str, 50)
    users = Set("Users")

    def __str__(self):
        return f"id = {self.role_id}; title = {self.title}"

    def __repr__(self):
        return f"id = {self.role_id}; title = {self.title}"


class Users(db.Entity):
    _table_ = "users"
    user_id = PrimaryKey(int, auto=True)
    email = Required(str, 50)
    password = Required(str, 20)
    age = Required(int)
    role = Required(Role, column="role_id")

    def __str__(self):
        return f"id = {self.user_id}; email = {self.email}"

    def __repr__(self):
        return f"email = {self.email}||||{self.role.title}"


db.generate_mapping(create_tables=False)
