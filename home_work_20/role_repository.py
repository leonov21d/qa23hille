from pony.orm import db_session, select

from home_work_20.models import Role


class BaseRepository:
    def __init__(self):
        self.model = None

    @db_session
    def get_by_id(self, id):
        entitu = self.model.get(lambda r: r.role_id == id)
        return entitu

    @db_session
    def delete_by_id(self, id):
        entity = self.get_by_id(id)
        entity.delete()




class RoleRepository(BaseRepository):
    def __init__(self):
        super().__init__()
        self.model = Role

    # @db_session
    # def get_roles_by_id(self, id):
    #     role = self.__model.get(lambda r: r.role_id == id)
    #     return role

    @db_session
    def get_all_by__lambda(self):
        roles = Role.select(lambda role: role)
        return roles.page(1).to_list()

    # @db_session
    # def delete_by_id(self, id):
    #     roles_to_delete = self.get_roles_by_id(id)
    #     roles_to_delete.delete()

    @db_session
    def get_all_by_sql(self):
        roles = self.model.select_by_sql("SELECT * FROM roles")
        return roles

    @db_session
    def get_all_by_cycle(self):
        roles = select(role for role in self.model).page(1).to_list()
        return roles

    # @db_session
    # def add_role(self, title):
    #     self.model(title=title)


if __name__ == "__main__":
    role_repo = RoleRepository()
    # role_repo.add_role("test2_role")
    # # role_repo.delete_by_id(3)
    # roles = role_repo.get_all_by__lambda()
    # # for role in roles:
    # #     print(role)
    # #
    # # roles = role_repo.get_all_by__lambda()
    # print(roles)
    result = role_repo.get_by_id(6)
    print(result)
