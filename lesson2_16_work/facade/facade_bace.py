from lesson2_16_work.page import AuthPage


class FacadeBase:
    def __init__(self):
        self._auth_page = AuthPage()
