class AuthUser:
    def __init__(self, email, password, remember, session):
        self.remember = remember
        self.email = email
        self.password = password
        self.session = session

    def user_auth(self):
        data = {
            "email": self.email,
            "password": self.password,
            "remember": self.remember

        }

        return self.session.post(url="https://qauto2.forstudy.space/api/auth/signin",
                                 json=data)


class Car:
    def __init__(self, car_brand_id, car_model_id, mileage, session):
        self.car_brand_id = car_brand_id
        self.car_model_id = car_model_id
        self.mileage = mileage
        self.session = session

    def add_car(self):
        data = {
            "carBrandId": self.car_brand_id,
            "carModelId": self.car_model_id,
            "mileage": self.mileage
        }
        result = self.session.post(url="https://qauto2.forstudy.space/api/cars", json=data)
        return result.json().get('data').get('id')

    def __str__(self):
        return f'car_brand_id: {self.car_brand_id}, car_model_id: {self.car_model_id}, mileage: {self.mileage}'

    def edit_car(self, car_id):
        data = {
            "carBrandId": self.car_brand_id,
            "carModelId": self.car_model_id,
            "mileage": self.mileage

        }
        return self.session.put(url=f"https://qauto2.forstudy.space/api/cars/{car_id}",
                                json=data)

    def delete_car(self, car_id):
        return self.session.delete(url=f"https://qauto2.forstudy.space/api/cars/{car_id}")

    def get_cars(self):
        list_cars = []
        result = self.session.get(url=f"https://qauto2.forstudy.space/api/cars")
        for car in result.json().get('data'):
            list_cars.append(car.get('id'))
            return list_cars
